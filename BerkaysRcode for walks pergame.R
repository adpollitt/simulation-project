install.packages('RODBC')

library (RODBC)
connection<-odbcConnect(dsn="mysql_analytics", uid="root", pwd="root")

query <- "select * from walkman"

walkman<- sqlQuery(connection, query, errors=True)

walkman

odbcClose(Connection)

view (walkman)


summary(walkman)


hist(runspergame$RunsPerGame)

hist(runspergame$RunsPerGame, breaks=100)
min(runspergame$RunsPerGame)
max(runspergame$RunsPerGame)
bins=seq(min(runspergame$RunsPerGame),max(runspergame$RunsPerGame),.05)
hist(runspergame$RunsPerGame)
plot(walkman$yearid,walkman$wp, main="Walks Per Game in 20 Years",ylab="Walks Per Game Avg",xlab="Years")
max(walkman$wp)
bins=seq(min(runspergame$Year),max(runspergame$Year),5)

