__author__ = 'Andrew Pollitt'
import fileinput
import urllib
import pymysql


def main():
    team_list = [
        "Angels",
        "Astros",
        "Athletics",
        "BlueJays",
        "Braves",
        "Brewers",
        "Cardinals",
        "Cubs",
        "D-backs",
        "Dodgers",
        "Giants",
        "Indians",
        "Mariners",
        "Marlins",
        "Mets",
        "Nationals",
        "Orioles",
        "Padres",
        "Phillies",
        "Pirates",
        "Rangers",
        "Rays",
        "Reds",
        "RedSox",
        "Rockies",
        "Royals",
        "Tigers",
        "Twins",
        "WhiteSox",
        "Yankees"
    ]

    f = open('addlikes.sql', 'w')
    for team in team_list:
        urllib.urlretrieve("http://graph.facebook.com/" + team, "TeamData\\" + team)
        json_file = fileinput.input("TeamData\\" + team)
        line = json_file.readline()
        number_of_likes = line.split('likes')[1].split(':')[1].split(',')[0]
        talking = line.split('talking_about_count')[1].split(':')[1].split(',')[0]
        try:
            team_name = line.split('of the ')[1]
        except IndexError:
            team_name = line.split('of your ')[1]
        if team == 'Cardinals':
            team_name = "St. Louis Cardinals"
        else:
            team_name = team_name.split('.')[0]
            team_name = team_name.split(',')[0]
            team_name = team_name.split(' who')[0]
        json_file.close()
        if team_name == "Miami Marlins":
            team_name = "Florida Marlins"

        sqls = "UPDATE `analytics`.`Teams` SET `likes` = " + number_of_likes + ", `talking_about` = " + talking + \
               " WHERE `name` = '" + team_name + "';\n"

        try:
            conn = pymysql.connect(host='127.0.0.1', port=3306, user='root', passwd='', db='MyArchDBS')
            cur = conn.cursor()
            cur.execute(sqls)
        except pymysql.err.OperationalError:
            f.write(sqls)


if __name__ == "__main__":
    main()